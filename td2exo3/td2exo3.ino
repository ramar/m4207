


void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  pinMode(3, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  analogWrite(3, sensorValue);
 // Serial.println(sensorValue);
  delay(1);        // delay in between reads for stability
}
