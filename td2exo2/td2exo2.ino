

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 7;     // the number of the pushbutton pin
const int ledPin =  6;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(6, OUTPUT);   // un actionneur (la LED)
  // initialize the pushbutton pin as an input:
  pinMode(7, INPUT);   //un capteur (le capteur)
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(7);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on:
    digitalWrite(6, HIGH);
  } else {
    // turn LED off:
    digitalWrite(6, LOW);
  }
}
