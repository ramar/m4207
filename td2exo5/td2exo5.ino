int pin = 13; 
volatile int state = LOW; // déclaration d'une variable volatile 
 
void setup() {  
  pinMode(13, OUTPUT);   
  attachInterrupt(0, blink, RISING); // attache l'interruption externe n°0 à la fonction blink  
}
 
void loop() {   
  digitalWrite(pin, state); // la LED reflète l'état de la variable  
}
 
void blink() // la fonction appelée par l'interruption externe n°0 
{   
 state = !state; // inverse l'état de la variable 
 } 
